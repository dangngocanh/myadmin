import React, { Component } from 'react';

class SearchForm extends Component {
    constructor(props) {
        super(props);
        this.state ={
            tempValue : ''
        }
    }
    isChage = (event)=> {
        // console.log(event.target.value)
        this.setState({
            tempValue : event.target.value
        })
        this.props.getInput(this.state.tempValue)
    }
    
    render() {
        return (
            <div className="col-sm-12 col-md-6">
                <div className=" d-flex">
                    <input type="text" onChange = {(event)=>this.isChage(event)}className="form-control col-sm-9 mr-2" id="searchForm" placeholder="Nhập thông tin ..." />
                    <button type="submit" className="btn btn-primary mb-2 col-auto" onClick ={(dl)=>this.props.getInput(this.state.tempValue)}>Tìm kiếm</button>
                </div>

            </div>
        );
    }
}

export default SearchForm;