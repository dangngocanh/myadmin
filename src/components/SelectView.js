import React, { Component } from 'react';

class SelectView extends Component {
    render() {
        return (
            <div className="col-sm-12 col-md-6">
                <div className="form-row align-items-center">
                    <label htmlFor="staticEmail" className="col-4 col-form-label text-center">Hiển thị
            cột xem</label>
                    <div className="col-auto my-1">
                        <select className="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option defaultChecked>Chọn</option>
                            <option value={5}>5</option>
                            <option value={10}>10</option>
                            <option value={15}>15</option>
                        </select>
                    </div>
                </div>
            </div>
        );
    }
}

export default SelectView;