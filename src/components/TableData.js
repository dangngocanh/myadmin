import React, { Component } from 'react';
import TableDataRow from './TableDataRow';

class TableData extends Component {
    delBtnClick =(userId) => {
      this.props.delUser(userId)
    }
    MapDataUser = () => 
        this.props.dataUserProps.map((value,key) => (
            // nhận tham số user rồi mỗi một phần tử đc lấy ra từ value truyền vao tham số user
            <TableDataRow 
            delBtnClick = {(userId) => this.delBtnClick(userId)}
            showE={() => this.props.showEdit()}
            editFunClick={(user) => this.props.editFun(value)}
            key = {key}
            id = {key}
            idDel = {value.id}
            name = {value.name}
            tel = {value.tel}
            address = {value.address}
            permission = {value.permission}
            />
        ))
/// this.props.editFun
// truyền tiếp vào table data row
    render() {
        //console.log(this.props.dataUserProps)
        return (
            <table className="table table-hover table-bordered table-sm ">
                <thead className="thead-dark ">
                    <tr>
                        <th scope="col" style={{ width: '5%' }}>STT</th>
                        <th scope="col" style={{ width: '25%' }}>Họ Và Tên</th>
                        <th scope="col">Quê Quán</th>
                        <th scope="col">SĐT</th>
                        <th scope="col">Quyền</th>
                        <th scope="col" className="text-center" style={{ width: '15%' }}>Thao Tác</th>
                    </tr>
                </thead>
                <tbody>
                
                    {this.MapDataUser()}
                </tbody>
            </table>
        );
    }
}

export default TableData;