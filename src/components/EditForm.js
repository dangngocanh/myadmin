import React, { Component } from 'react';

class EditForm extends Component {
constructor(props) {
    super(props);
    this.state ={
        id : this.props.editUser.id,
        name : this.props.editUser.name,
        tel : this.props.editUser.tel,
        address : this.props.editUser.address,
        permission : this.props.editUser.permission
    }
}
isChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
        [name] : value
    });
}

// sẽ chứa this.props.getValueEdit
saveBtn = () => {
    var info = {};
    info.id = this.state.id;
    info.name = this.state.name;
    info.tel = this.state.tel;
    info.address = this.state.address;
    info.permission = this.state.permission;
    this.props.getValueEdit(info);
    this.props.isShowEdit();
    
}
    render() {
        
        return (
            <form>   
            <div className="form-row">
               
            <div className="form-group col-md-3">
                <label>Tên</label>
                <input type="text" name ="name" defaultValue ={this.props.editUser.name} onChange = {(event)=> this.isChange(event)} className="form-control" id="nameForm" placeholder="Họ Và Tên" />
            </div>
            <div className="form-group col-md-3">
                <label >Quê Quán</label>
                <input defaultValue ={this.props.editUser.address} type="text" name ="address" onChange = {(event)=> this.isChange(event)} className="form-control" id="queForm" placeholder="Địa chỉ" />
            </div>
            <div className="form-group col-md-3">
                <label>SDT</label>
                <input type="text" defaultValue ={this.props.editUser.tel} name ="tel" onChange = {(event)=> this.isChange(event)} className="form-control" id="sdtForm" placeholder="SĐT" />
            </div>

            <div className="form-group col-md-3">
                <label>Quyền</label>
                <div className="form-row">
                    <div className="col-md-6">
                        <select name ="permission" defaultValue ={this.props.editUser.permission} onChange = {(event)=> this.isChange(event)} className="custom-select" id="inlineFormCustomSelect">
                            <option defaultChecked>Chọn quyền</option>
                            <option value="1">Admin</option>
                            <option value="2">Khách</option>
                        </select>
                    </div>
                    <div className="col-md-6">
                        <input type="button" 
                        onClick={() => this.saveBtn()} className="btn btn-success form-control " value ="Lưu"/>
                    </div>
                </div>
            </div>
            
        </div> </form>
        );
    }
}

export default EditForm;