import React, { Component } from 'react';

class AddForm extends Component {
    constructor(props) {
        super(props);
        this.state ={
            statusEdit : false,
            id: "",
            name: "",
            tel: "",
            address: "",
            permission: "",
        }
    }
isChane = (event) => {
    const name = event.target.name;
    const value = event.target.value;
   this.setState({
       // lưu thành 1 mảng  object
       [name] : value
   })
   // đóng gói thành item

//    console.log(item)
}

    changeStatus = () => {
        this.setState({
            statusEdit : !this.state.statusEdit
        })
    }
    //show edit
    showBtnEdit = () =>{
        if(this.state.statusEdit === true){
            return <button className="btn btn-sm btn-danger"  onClick={() => this.changeStatus()}>Đóng lại</button>
        }else {
            return <button className="btn btn-sm btn-info mr-2" onClick={() => this.changeStatus()}>Thêm mới</button>;
            
        }
    }
    
    //show form
    showForm =()=> {
        if(this.state.statusEdit === true){
            return (   <form>   
                <div className="form-row">
                   
                <div className="form-group col-md-3">
                    <label>Tên</label>
                    <input type="text" name ="name" onChange = {(event)=> this.isChane(event)} className="form-control" id="nameForm" placeholder="Họ Và Tên" />
                </div>
                <div className="form-group col-md-3">
                    <label >Quê Quán</label>
                    <input type="text" name ="address" onChange = {(event)=> this.isChane(event)} className="form-control" id="queForm" placeholder="Địa chỉ" />
                </div>
                <div className="form-group col-md-3">
                    <label>SDT</label>
                    <input type="text" name ="tel" onChange = {(event)=> this.isChane(event)} className="form-control" id="sdtForm" placeholder="SĐT" />
                </div>

                <div className="form-group col-md-3">
                    <label>Quyền</label>
                    <div className="form-row">
                        <div className="col-md-6">
                            <select name ="permission" onChange = {(event)=> this.isChane(event)} className="custom-select" id="inlineFormCustomSelect">
                                <option defaultChecked>Chọn quyền</option>
                                <option value="1">Admin</option>
                                <option value="2">Khách</option>
                            </select>
                        </div>
                        <div className="col-md-6">
                            <input type="reset" 
                            onClick={(name,tel,address,permission) => this.props.addData(this.state.name,this.state.tel,this.state.address,this.state.permission)} className="btn btn-success form-control " value ="Thêm"/>
                        </div>
                    </div>
                </div>
                
            </div> </form>
            )
        }
    }
    render() {
     // console.log(this.state.permission)
        return (
            <div>
            {this.showBtnEdit()}
            {this.showForm()}
           
            
            </div>
        );
    }
}

export default AddForm;