

import './../App.css';
import Header from './Header';
import NavLeft from './NavLeft';
import Main from './Main';

import React, { Component } from 'react';

class App extends Component {
  
  render() {
    
    return (
      <div>
        <Header/>
        <section>
          <div className="container-fluid">
            <div className="row">
  
              <NavLeft/>
              <Main/>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default App;


