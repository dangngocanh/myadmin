import React, { Component } from 'react';

class NavLeft extends Component {
    render() {
        return (
            <div className="col-12 col-md-2 bg-dark ">
                <nav className="navbar-dark navbar">
                    <ul className="sidebar navbar-nav ">
                        <li className="nav-item">
                            <a className="nav-link" href="index.html">
                                <i className="fas fa-fw fa-tachometer-alt" />
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li className="nav-item ">
                            <a className="nav-link active" href="tables.html">
                                <i className="fa fa-table" />
                                <span>Tables</span></a>
                        </li>
                    </ul>
                </nav>
            </div>

        );
    }
}

export default NavLeft;