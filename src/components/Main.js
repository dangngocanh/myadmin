import React, { Component } from 'react';
import AddForm from './AddForm';
import SelectView from './SelectView';
import SearchForm from './SearchForm';
import TableData from './TableData';
import Pagination from './Pagination';
import DataUser from './Data.json';
import EditForm from './EditForm';

const uuidv1 = require('uuid/v1');

class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
          data :[],
          searchText: '',
          editStatus : false,
          // truyền state editUser thành 1 object
          editUser : {},
          objEditInfo : {}

        }
      }

      
      componentWillMount() {
          if (localStorage.getItem('userData') === null) {
            localStorage.setItem('userData',JSON.stringify(DataUser))
          }else{
              var temp = JSON.parse(localStorage.getItem('userData'));
              this.setState({
                  data: temp
              });
          }
      }
    getValueEdit = (info) => {
       // console.log('bạn vừa sửa ' + info.name)
        this.setState({
            objEditInfo : info
        })
        this.state.data.forEach((value,key) => {
            if(value.id === info.id) {
                value.name = info.name;
                value.address = info.address;
                value.tel = info.tel;
                value.permission = info.permission;
            }
        })
        localStorage.setItem("userData",JSON.stringify(this.state.data))
    }
      // if show
    editShow = () => {
        if(this.state.editStatus === true) {
            return (<EditForm getValueEdit={(info) => this.getValueEdit(info)} editUser = {this.state.editUser} isShowEdit = {() => this.showEdit()}/>)
        }
    }
    //
    delUser = (userId) => {
     //   console.log('đây là id: '+ userId)
        //dùngf filer để lọc ra phần tử cần loại bỏ
      var  delData = this.state.data.filter(item => item.id !== userId);
      this.setState({
        data : delData
      });
      localStorage.setItem("userData",JSON.stringify(delData))
        console.log(delData);
        // duyệt từng phần tử để láy ra giá trị dùng: forEach
        // delData.forEach((value,key) => {
        //     if(value.id == userId){
        //         console.log('trùng nhau in Name :' + value.name)
        //     }
        // })
        }
    // thay đổi state 
    showEdit = () => {
        this.setState({
            editStatus : !this.state.editStatus
        })
    }
    // truyền xuống thông qua props  gửi lên là tham số truyền vào
    // lấy ra giá trị từng li thành user
    editUser = (user)=>{
         //console.log(user);
         //setState gán editUser bằng user
         this.setState({
            editUser : user
         })
     }
      getNewData = (name,tel,address,permission) =>{
             var item = {};
            item.id = uuidv1();
            item.name =name;
            item.tel = tel;
            item.address =address;
            item.permission=permission;
            var items = this.state.data;
            this.setState({
                data: items
            })
            
            items.push(item)
            localStorage.setItem("userData",JSON.stringify(items))
      }
      // lấy giá trị input phần tìm kiếm 
      getValueInput = (dl) => {
          this.setState({
              searchText:dl
          });
         // console.log("du lieu nhan dc là : " +this.state.searchText);
      }
    render() {
        var ketqua = [];
        this.state.data.forEach((item) => {
            if(item.name.indexOf(this.state.searchText) !== -1){
                ketqua.push(item);
            }
        })
       // localStorage.setItem('userData',JSON.stringify(DataUser))
       // console.log(ketqua)
        return (
            <div className="col-12 col-md-10">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item">
                        <a href="/">Dashboard</a>
                    </li>
                    <li className="breadcrumb-item active">Tables</li>
                </ol>
                <div className="table-responsive-sm">
                    {/* ADD Form */}
                    <AddForm addData={(name,tel,address,permission) => this.getNewData(name,tel,address,permission)}/>                   
                    <div className="row">
                        {/* chọn view hiển thị */}
                        <SelectView/>
                        <SearchForm getInput = {(dl)=> this.getValueInput(dl)} />
                        <div className="col-12">
                            {this.editShow()}
                        </div>
                        
                    </div>
                    {/* bảng dữ liệu */}
                    <TableData delUser={(userId) => this.delUser(userId)} editFun={(user) => this.editUser(user)} dataUserProps = {ketqua} showEdit = {() => this.showEdit()}/>
                </div>
                <Pagination/>
            </div>

        );
    }
}

export default Main;