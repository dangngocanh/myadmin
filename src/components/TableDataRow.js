import React, { Component } from 'react';

class TableDataRow extends Component {
    showPermission = () => {
        if(this.props.permission == 1){
            return "Admin"
        }else {
            return "Khách"
        }
    }
    delBtnClick =(userId) => {
        this.props.delBtnClick(userId)
    }
    editClick =()=>{
        this.props.editFunClick();
        this.props.showE();
    }
    render() {
        // lấy ra props.editFunClick
        return (
            <tr>
                <th scope="row">{this.props.id}</th>
                <td>{this.props.name}</td>
                <td>{this.props.tel}</td>
                <td>{this.props.address}</td>
                <td>{this.showPermission()}</td>
                <td className="text-center">
                    <button type="button" onClick={() => this.editClick()} className="btn btn-success btn-sm"> <i className="fa fa-pencil" aria-hidden="true" />Sửa</button>
                    <button type="button" onClick={(userId) =>this.delBtnClick(this.props.idDel)} className="btn btn-danger btn-sm"><i className="fa fa-trash-o" aria-hidden="true" />Xóa</button>
                </td>
            </tr>
        );
    }
}

export default TableDataRow;